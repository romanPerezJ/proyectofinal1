package  com.example.recyclerview.adapters.viewholders


import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.listeners.BookListener
import com.example.recyclerview.models.Book
import com.example.recyclerview.R

class BookViewHolder(val itemView2: View) : RecyclerView.ViewHolder(itemView2) {

    private var bookListener: BookListener? = null
    private var book:Book? = null

    private var titleBookTextView: TextView = itemView2.findViewById(R.id.titleBookTextView)
    private var pagesBookTextView: TextView = itemView2.findViewById(R.id.pagesBookTextView)
    private var authorBookTextView: TextView = itemView2.findViewById(R.id.authorBookTextView)
    private var publisherBookTextView: TextView = itemView2.findViewById(R.id.publisherBookTextView)
    private var shareButton: Button = itemView2.findViewById(R.id.shareButton)

    fun bindBook(book: Book) {
        this.book=book
        titleBookTextView.text = this.book?.title
        pagesBookTextView.text = this.book?.pages.toString()
        authorBookTextView.text = this.book?.author
        publisherBookTextView.text = this.book?.publisher

        itemView2.setOnClickListener { view ->
            bookListener?.onClickBook(this.book!!)
        }
        shareButton.setOnClickListener { view ->
            bookListener?.onShareBook(this.book!!)
        }
    }

    fun setBookListener(listener: BookListener?){
        this.bookListener=listener
    }
}