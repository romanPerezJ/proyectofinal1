package com.example.recyclerview.fragments

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.DetailActivity
import com.example.recyclerview.R
import com.example.recyclerview.adapters.BookAdapter
import com.example.recyclerview.listeners.BookListener
import com.example.recyclerview.models.Book
import kotlinx.android.synthetic.main.fragment_main.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() , BookListener{
    // TODO: Rename and change types of parameters

    private var bookAdapter: BookAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            /** param1 = it.getString(ARG_PARAM1)
            * param2 = it.getString(ARG_PARAM2)
            **/
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_main, menu)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val books = arrayListOf<Book>(
                Book(
                        1,
                        "Codigo Da Vinci",
                        "Editorial",
                        468,
                        "Dan Brown"
                ),
                Book(
                        2,
                        "Angeles y Demonios",
                        "Editorial",
                        234,
                        "Dan Brown"
                ),
                Book(
                        3,
                        "Inferno",
                        "Editorial",
                        445,
                        "Dan Brown"
                ),
                Book(
                        4,
                        "El Señor de los Anillos",
                        "Editorial",
                        134,
                        "Philps Coulsen"
                ),
                Book(
                        5,
                        "El Señor de los Anillos: Retorno del Rey",
                        "Editorial",
                        4567,
                        "Philps Coulsen"
                )
        )
        bookAdapter = BookAdapter(books)
        bookAdapter?.setBookListener(this)
        bookAdapter?.let {
            booksRecyclerView.adapter = it
        }
        val layoutManager = LinearLayoutManager(
                requireContext(),
                RecyclerView.VERTICAL, false
        )
        booksRecyclerView.layoutManager = layoutManager
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.registerBookItem -> {
                //startActivityForResult(Intent(context,RegisterOkActivity::class.java),RESULT_REGISTER_BOOK)
                return true
            }
            R.id.addBooItem -> {
                val book = Book(1212,"EL rio que fluye","McGrawHill",321,"Paulo Cohelo")
                bookAdapter?.updateBook(book,1)
                return true
            }
            R.id.cleanBooksItem -> {
                bookAdapter?.cleanBooks()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MainFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            MainFragment()
    }

    override fun onClickBook(book: Book) {
        val isTablet= resources.getBoolean(R.bool.isTablet)
        if(isTablet){
            requireFragmentManager()
                .beginTransaction()
                .replace(R.id.detailContainer,DetailBookFragment.newInstance(book))
                .commit()
        }else{
            val detailIntent = Intent(requireContext(), DetailActivity::class.java).apply {
                val detailBundle = Bundle().apply {
                    putParcelable("book",book)
                }
                putExtras(detailBundle)
            }
            startActivity(detailIntent)
        }
    }

    override fun onShareBook(book: Book) {
        TODO("Not yet implemented")
    }
}