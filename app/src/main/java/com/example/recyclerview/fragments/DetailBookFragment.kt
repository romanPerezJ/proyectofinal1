package com.example.recyclerview.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.recyclerview.R
import com.example.recyclerview.models.Book
import kotlinx.android.synthetic.main.fragment_detail_book.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_BOOK = "book"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailBookFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailBookFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var book: Book? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
           book=it.getParcelable(ARG_BOOK)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_book, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        book?.let {
            /*detailBookListener?.onShowTitle(it.title)*/
            titleBookDetailTextView.text = it.title
            authorBookDetailTextView.text = it.author
            publisherBookDetailTextView.text = it.publisher
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailBookFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(book: Book) =
            DetailBookFragment().apply {
                arguments = Bundle().apply {
                   putParcelable(ARG_BOOK,book)
                    /*putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)*/
                }
            }
    }
}