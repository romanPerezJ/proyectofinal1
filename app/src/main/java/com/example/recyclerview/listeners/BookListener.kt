package com.example.recyclerview.listeners

import com.example.recyclerview.models.Book

interface BookListener {

    fun onClickBook(book: Book)

    fun onShareBook(book: Book)
}