package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recyclerview.fragments.DetailBookFragment
import com.example.recyclerview.models.Book

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        val book = intent.getParcelableExtra<Book>("book")

        book?.let {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.detailContainer, DetailBookFragment.newInstance(it))
                .commit()
        }
    }
}